#include <stdio.h>
#include "Application.h"

int main() {

    Application* app = new Application();

    return app->run();
}
