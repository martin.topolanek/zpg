#define PI 3.14159265358979323846
#include "Camera.h"

Camera::Camera(glm::vec3 aEye, glm::vec3 aUp, float aFi, float aPsi)
{
    oEye = aEye;
    oUp  = aUp;

    oFi  = aFi;
    oPsi = aPsi;

    UpdateListeners();
}

Camera::~Camera()
{

}

glm::vec3 Camera::GetPosition() const {
    return oEye;
}

glm::vec3 Camera::GetDirection() const {
    return GetDirection(oFi, oPsi);
}

glm::vec3 Camera::GetDirection(float aFi, float aPsi) const {
    return glm::vec3(cos(aFi / 180.0 * PI) * cos(aPsi / 180.0 * PI), sin(aPsi / 180.0 * PI), sin(aFi / 180.0 * PI) * cos(aPsi / 180.0 * PI));
}

glm::mat4 Camera::GetTransformMatrix() const {
    glm::vec3 lDirection = GetDirection();

    return glm::lookAt(oEye, oEye + lDirection, oUp);
}

glm::mat4 Camera::GetProjectionMatrix() const {
    return glm::perspective(oProjectionAngle, (float) Window::getWindow()->GetWidth() / Window::getWindow()->GetHeight(), 0.1f, 100.0f);
}

void Camera::MoveForward(float aShift) {
    glm::vec3 lDirection = GetDirection();

    oEye += glm::normalize(lDirection) * aShift;

    UpdateListeners();
}

void Camera::MoveToSide(float aShift) {
    float lAngleRad = oFi / 180.0 * PI;
	glm::vec3 lDirection = glm::vec3(cos(lAngleRad), sin(lAngleRad), sin(lAngleRad));

	glm::vec3 lSideVector = -glm::cross(oUp, lDirection);

	oEye += glm::normalize(lSideVector) * aShift;

	UpdateListeners();
}

void Camera::AddListener(CameraListener* aListener) {
    oListeners.push_back(aListener);
}

void Camera::RemoveListener(CameraListener* aListener) {
    std::vector<CameraListener*>::iterator lSearchedItem = std::find(oListeners.begin(), oListeners.end(), aListener);

    if (lSearchedItem != oListeners.end()) {
        oListeners.erase(lSearchedItem);
    }
}

void Camera::UpdateListeners() {
    for (unsigned int i = 0; i < oListeners.size(); i++) {
        oListeners[i]->UpdateCamera(this);
    }
}

void Camera::ClockTick() {
    MoveForward(oForwardBackwardSpeed);
    MoveToSide(oLeftRightSpeed);
}

void Camera::KeyPressed(int aKey) {
    switch(aKey) {
    case GLFW_KEY_W:
        oForwardBackwardSpeed += 0.1;
        break;
    case GLFW_KEY_S:
        oForwardBackwardSpeed -= 0.1;
        break;
    case GLFW_KEY_A:
        oLeftRightSpeed -= 0.1;
        break;
    case GLFW_KEY_D:
        oLeftRightSpeed += 0.1;
        break;
    default:
        break;
    }
}

void Camera::KeyReleased(int aKey) {
    switch(aKey) {
    case GLFW_KEY_W:
        oForwardBackwardSpeed -= 0.1;
        break;
    case GLFW_KEY_S:
        oForwardBackwardSpeed += 0.1;
        break;
    case GLFW_KEY_A:
        oLeftRightSpeed += 0.1;
        break;
    case GLFW_KEY_D:
        oLeftRightSpeed -= 0.1;
        break;
    default:
        break;
    }
}

void Camera::MouseMoved(double aPrevXPos, double aXPos, double aPrevYPos, double aYPos) {
    oFi  += oMouseSpeed * float(aXPos - aPrevXPos);
    oPsi += oMouseSpeed * float(aPrevYPos - aYPos);

    if (oPsi > 89) {
        oPsi = 89;
    }
    if (oPsi < -89) {
        oPsi = -89;
    }
}

void Camera::MouseScrolled(double aYOffset) {
    //oProjectionAngle -= 2 * aYOffset;
}
