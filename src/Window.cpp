#define GLEW_STATIC
#include "Window.h"
#include "Includes.h"

Window* Window::oWindow = nullptr;
Window::Window()
{
    /*
    glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, 3);
    glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, 3);
    glfwWindowHint(GLFW_OPENGL_FORWARD_COMPAT, GL_TRUE);
    glfwWindowHint(GLFW_OPENGL_PROFILE, GLFW_OPENGL_CORE_PROFILE);
    */

    oGLFWWindow = glfwCreateWindow(800, 600, "ZPG", NULL, NULL);
    if (!oGLFWWindow)
    {
        glfwTerminate();
    }

    glfwMakeContextCurrent(oGLFWWindow);
    glfwSwapInterval(1);

    glewExperimental = GL_TRUE;
    glewInit();

    // get version info
    printf("OpenGL Version: %s\n", glGetString(GL_VERSION));
    printf("Using GLEW %s\n", glewGetString(GLEW_VERSION));
    printf("Vendor %s\n", glGetString(GL_VENDOR));
    printf("Renderer %s\n", glGetString(GL_RENDERER));
    printf("GLSL %s\n", glGetString(GL_SHADING_LANGUAGE_VERSION));

    glfwGetFramebufferSize(oGLFWWindow, &width, &height);
    glViewport(0, 0, width, height);
}

void Window::InitCursors() {
    glfwGetCursorPos(oGLFWWindow, &oXPos, &oYPos);
    glfwSetCursorPos(oGLFWWindow, width/2, height/2);
}

void Window::version_info() {
    int major, minor, revision;
    glfwGetVersion(&major, &minor, &revision);
    printf("Using GLFW %i.%i.%i\n", major, minor, revision);
}

GLFWwindow* Window::getGLFWWindow() {
    return oGLFWWindow;
}

Window* Window::getWindow() {
    if (oWindow == nullptr) {
        oWindow = new Window();
    }
    return oWindow;
}

int Window::shouldClose() {
    if (oGLFWWindow != NULL)
        return glfwWindowShouldClose(oGLFWWindow);
    else
        return 1;
}

int Window::GetHeight() {
    return height;
}

int Window::GetWidth() {
    return width;
}

float Window::GetXPos() {
    return oXPos;
}

float Window::GetYPos() {
    return oYPos;
}
