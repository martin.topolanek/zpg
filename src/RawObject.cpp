#include "RawObject.h"

RawObject::RawObject(std::string aModelID)
{
    this->oModel = ModelManager::GetModelManager()->GetRawModel(aModelID);
    this->oVerticesAmount = this->oModel->GetVerticesAmount();
    this->oVAO = this->oModel->GetVAO();
}

void RawObject::Draw() const {
    oShaderProgram->Draw(GetTransformationMatrix(), this->oVAO, this->oVerticesAmount);
}
