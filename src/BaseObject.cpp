#include "BaseObject.h"
#include <glm/gtc/matrix_transform.hpp>

BaseObject::BaseObject()
{
    oPosition = glm::vec3(0.0f, 0.0f, 0.0f);
    oRotationMatrix = glm::mat4(1.0f);
    oScaleMatrix = glm::mat4(1.0f);

}

BaseObject::~BaseObject()
{

}

glm::mat4 BaseObject::GetTransformationMatrix() const {
	return glm::translate(glm::mat4(1.0f), oPosition) * oRotationMatrix * oScaleMatrix;

}

void BaseObject::Rotate(const glm::vec3& aRotationVector, float aAngle) {
    oRotationMatrix = glm::rotate(glm::mat4(1.0f), aAngle, aRotationVector) * oRotationMatrix;
}

void BaseObject::RotateX(float aAngle) {
    Rotate(glm::vec3(1.0f, 0.0f, 0.0f), aAngle);
}

void BaseObject::RotateY(float aAngle) {
    Rotate(glm::vec3(0.0f, 1.0f, 0.0f), aAngle);
}

void BaseObject::RotateZ(float aAngle) {
    Rotate(glm::vec3(0.0f, 0.0f, 1.0f), aAngle);
}

void BaseObject::SetScale(float aConst) {
    oScaleMatrix = glm::scale(glm::mat4(1.0f), glm::vec3(aConst));
}

void BaseObject::SetPosition(const glm::vec3& aPosition) {
	oPosition = aPosition;
}

void BaseObject::SetShadingProgram(const Shader* aShadingProgram) {
    oShaderProgram = aShadingProgram;
}
