#include "TextureManager.h"
#include "SOIL/SOIL.h"

TextureManager TextureManager::oTextureManager;

TextureManager::TextureManager()
{

}

TextureManager* TextureManager::getTextureManager() {
    return &oTextureManager;
}

GLuint TextureManager::SaveTexture(const char* aPath) {
    //Generate texture ID and load texture data
    GLuint lTextureID;
    glGenTextures( 1, &lTextureID );

    int lImageWidth, lImageHeight;

    unsigned char *lImage = SOIL_load_image( aPath, &lImageWidth, &lImageHeight, 0, SOIL_LOAD_RGB );

    // Assign texture to ID
    glBindTexture( GL_TEXTURE_2D, lTextureID );
    glTexImage2D( GL_TEXTURE_2D, 0, GL_RGB, lImageWidth, lImageHeight, 0, GL_RGB, GL_UNSIGNED_BYTE, lImage );
    glGenerateMipmap( GL_TEXTURE_2D );

    // Parameters
    glTexParameteri( GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT );
    glTexParameteri( GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT );
    glTexParameteri( GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR_MIPMAP_LINEAR );
    glTexParameteri( GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR );
    glBindTexture( GL_TEXTURE_2D,  0);

    SOIL_free_image_data( lImage );

    return lTextureID;
}

GLuint TextureManager::SaveCubeTexture(std::vector<std::string> aFaces) {
    GLuint lTextureID;
    glGenTextures( 1, &lTextureID );

    int lImageWidth, lImageHeight, lChannel;
    unsigned char *lImage;

    glBindTexture( GL_TEXTURE_CUBE_MAP, lTextureID );

    for ( GLuint i = 0; i < aFaces.size( ); i++ )
    {
        const char* lPath = aFaces[i].c_str();
        lImage = SOIL_load_image( lPath, &lImageWidth, &lImageHeight, &lChannel, SOIL_LOAD_RGB );
        glTexImage2D( GL_TEXTURE_CUBE_MAP_POSITIVE_X + i, 0, GL_RGB, lImageWidth, lImageHeight, 0, GL_RGB, GL_UNSIGNED_BYTE, lImage );
        SOIL_free_image_data( lImage );

    }

    glTexParameteri( GL_TEXTURE_CUBE_MAP, GL_TEXTURE_MAG_FILTER, GL_LINEAR );
    glTexParameteri( GL_TEXTURE_CUBE_MAP, GL_TEXTURE_MIN_FILTER, GL_LINEAR );
    glTexParameteri( GL_TEXTURE_CUBE_MAP, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE );
    glTexParameteri( GL_TEXTURE_CUBE_MAP, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE );
    glTexParameteri( GL_TEXTURE_CUBE_MAP, GL_TEXTURE_WRAP_R, GL_CLAMP_TO_EDGE );
    glBindTexture( GL_TEXTURE_CUBE_MAP, 0);

    return lTextureID;
}

void TextureManager::CreateTexture(std::string aPath) {
    const char* lPath = aPath.c_str();
    oTextureIDs[aPath] = SaveTexture(lPath);
}

void TextureManager::CreateTexture(std::string aPath, std::string aTextureID) {
    const char* lPath = aPath.c_str();
    oTextureIDs[aTextureID] = SaveTexture(lPath);
}

void TextureManager::CreateCubeTexture(std::vector<std::string> aFaces, std::string aTextureID) {
    oTextureIDs[aTextureID] = SaveCubeTexture(aFaces);
}

GLuint TextureManager::GetTexture(std::string aTextureID) {
    if (oTextureIDs.count(aTextureID) > 0) {
        return oTextureIDs[aTextureID];
    }
    return (GLuint) 0;
}
