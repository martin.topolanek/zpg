#include "AssimpObject.h"

AssimpObject::AssimpObject()
{

}

AssimpObject::AssimpObject(std::string aModelID) {
    this->oModel = ModelManager::GetModelManager()->GetAssimpModel(aModelID);
    this->oMeshes = this->oModel->GetMeshes();
}

void AssimpObject::Draw() const {
    oShaderProgram->Draw(GetTransformationMatrix(), &oMeshes);
}
