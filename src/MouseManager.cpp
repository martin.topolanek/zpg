#include "MouseManager.h"

MouseManager MouseManager::oMouseManager;

MouseManager::MouseManager()
{
    //ctor
}

MouseManager::~MouseManager()
{
    //dtor
}

void MouseManager::MouseMoved(double aXPos, double aYPos) {
    for (unsigned int i = 0; i < oListeners.size(); i++) {
        oListeners[i]->MouseMoved(oPrevXPos, aXPos, oPrevYPos, aYPos);
    }

    oPrevXPos = aXPos;
    oPrevYPos = aYPos;
}

void MouseManager::MouseScrolled(double aYOffset) {
    for (unsigned int i = 0; i < oListeners.size(); i++) {
        oListeners[i]->MouseScrolled(aYOffset);
    }
}

void MouseManager::AddListener(MouseListener* aListener) {
    if (aListener != nullptr) {
        oListeners.push_back(aListener);
    }
}

MouseManager* MouseManager::GetMouseManager() {
    return &oMouseManager;
}
