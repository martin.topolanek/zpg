#include "RawModel.h"

RawModel::RawModel(std::vector<Vertex> aVertices)
{
    this->LoadModel(aVertices);
}

void RawModel::AttachTexture(std::string aPath, TextureType aTextureType) {
    TextureManager* lTexManager = TextureManager::getTextureManager();

    lTexManager->CreateTexture(aPath);
    this->oTextures[aTextureType] = lTexManager->GetTexture(aPath);
}

GLuint RawModel::GetTextureByType(TextureType aTextureType) {
    return this->oTextures[aTextureType];
}

GLuint RawModel::GetVerticesAmount() {
    return this->oVerticesCount;
}

GLuint RawModel::GetVAO() {
    return this->oVAO;
}

void RawModel::LoadModel(std::vector<Vertex> aVertices) {

    glGenVertexArrays(1, &oVAO);
    glBindVertexArray(oVAO);
    glEnableVertexAttribArray(0);
    glEnableVertexAttribArray(1);
    glEnableVertexAttribArray(2);

    glGenBuffers(1, &oVBO);
    glBindBuffer(GL_ARRAY_BUFFER, oVBO);
    glBufferData(GL_ARRAY_BUFFER, aVertices.size() * sizeof(aVertices[0]), &aVertices[0], GL_STATIC_DRAW);

    glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, sizeof(aVertices[0]), (GLvoid*)0);
    glVertexAttribPointer(1, 3, GL_FLOAT, GL_FALSE, sizeof(aVertices[0]), (GLvoid*)sizeof(aVertices[0].Position));
    glVertexAttribPointer(2, 3, GL_FLOAT, GL_FALSE, sizeof(aVertices[0]), (GLvoid*)sizeof(aVertices[0].Normal));

    this->oVerticesCount = aVertices.size();
}


