#include "LightStorage.h"
LightStorage LightStorage::oLightStorageManager;

LightStorage::LightStorage()
{

}

LightStorage* LightStorage::GetLightStorageManager() {
    return &oLightStorageManager;
}

void LightStorage::AddLight(Light* aLight) {

    switch(aLight->GetLightType()) {
    case DIRECTION_LIGHT: CreateDirectionalLight(aLight); break;
    case POINT_LIGHT: CreatePointLight(aLight); break;
    case SPOT_LIGHT: CreateSpotLight(aLight); break;
    default: std::cout << "Unknown type of light." << std::endl;
    }
}

void LightStorage::RemoveLight(Light* aLight) {
    /* TO DO */
}

void LightStorage::CreateDirectionalLight(Light* aLight) {
    if (oLightStorage.count(DIRECTION_LIGHT) > 0) {
        if (oLightStorage[DIRECTION_LIGHT].size() > MAX_DIRECTION_LIGHTS) {
            std::cout << "Max number of direction lights exceeded." << std::endl;
            return;
        }
    }

    this->oLightStorage[DIRECTION_LIGHT].push_back(aLight);
}

void LightStorage::CreatePointLight(Light* aLight) {
    if (oLightStorage.count(POINT_LIGHT) > 0) {
        if (oLightStorage[POINT_LIGHT].size() > MAX_POINT_LIGHT) {
            std::cout << "Max number of point lights exceeded." << std::endl;
            return;
        }
    }

    this->oLightStorage[POINT_LIGHT].push_back(aLight);
}

void LightStorage::CreateSpotLight(Light* aLight) {
    if (oLightStorage.count(SPOT_LIGHT) > 0) {
        if (oLightStorage[SPOT_LIGHT].size() > MAX_SPOT_LIGHT) {
            std::cout << "Max number of point lights exceeded." << std::endl;
            return;
        }
    }

    this->oLightStorage[SPOT_LIGHT].push_back(aLight);
}

std::vector<Light*> LightStorage::GetDirectionLights() {
    if (this->oLightStorage.count(DIRECTION_LIGHT) > 0) {
        return this->oLightStorage[DIRECTION_LIGHT];
    }

    return std::vector<Light*>();
}

std::vector<Light*> LightStorage::GetPointLights() {
    if (this->oLightStorage.count(POINT_LIGHT) > 0) {
        return this->oLightStorage[POINT_LIGHT];
    }

    return std::vector<Light*>();
}

std::vector<Light*> LightStorage::GetSpotLights() {
    if (this->oLightStorage.count(SPOT_LIGHT) > 0) {
        return this->oLightStorage[SPOT_LIGHT];
    }

    return std::vector<Light*>();
}
