#include "KeyboardManager.h"

KeyboardManager KeyboardManager::oKeyboardManager;

KeyboardManager::KeyboardManager()
{

}

KeyboardManager::~KeyboardManager()
{

}

void KeyboardManager::KeyPressed(int aKey) {
    if (oPressedKeys.count(aKey) == 0) {
        oPressedKeys.insert(aKey);

        for (unsigned int i = 0; i < oListeners.size(); i++) {
            oListeners[i]->KeyPressed(aKey);
        }
    }
}

void KeyboardManager::KeyReleased(int aKey) {
    if (oPressedKeys.count(aKey) > 0) {
        oPressedKeys.erase(aKey);

        for(unsigned int i = 0; i < oListeners.size(); i++) {
            oListeners[i]->KeyReleased(aKey);
        }
    }
}

bool KeyboardManager::IsPressed(int aKey) {
    return oPressedKeys.count(aKey) > 0;
}

void KeyboardManager::AddListener(KeyboardListener* aListener) {
    if (aListener != nullptr) {
        oListeners.push_back(aListener);
    }
}

KeyboardManager* KeyboardManager::GetKeyboardManager() {
    return &oKeyboardManager;
}
