#include "MouseListener.h"

MouseListener::MouseListener()
{
    MouseManager::GetMouseManager()->AddListener(this);
}

MouseListener::~MouseListener()
{

}

void MouseListener::MouseMoved(double aPrevXPos, double aXPos, double aPrevYPos, double aYPos) {

}

void MouseListener::MouseScrolled(double aYOffset) {

}
