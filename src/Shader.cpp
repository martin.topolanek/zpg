#pragma once
#include "Shader.h"

static std::string GLuintToStr(GLuint value) {
    stringstream ss;

    ss << value;

    return ss.str();
}

Shader::Shader(std::string aVertexShaderFileName, std::string aFragmentShaderFileName, std::string aShaderProgramName, int aMaxLights) {

	oShaderStorage.oVertexShaderFileName = aVertexShaderFileName;
	oShaderStorage.oFragmentShaderFileName = aFragmentShaderFileName;
	oShaderStorage.oProgramName = aShaderProgramName;

	oShaderStorage.oProgramID = ShaderLoader::loadShader(aVertexShaderFileName.c_str(), aFragmentShaderFileName.c_str());

    oShaderStorage.oModelMatrixUniformLocation = glGetUniformLocation(oShaderStorage.oProgramID, "ModelMatrix");
    oShaderStorage.oViewMatrixUniformLocation  = glGetUniformLocation(oShaderStorage.oProgramID, "ViewMatrix");
    oShaderStorage.oProjectionMatrixUniformLocation = glGetUniformLocation(oShaderStorage.oProgramID, "ProjectionMatrix");
    oShaderStorage.oViewPositionUniformLocation = glGetUniformLocation(oShaderStorage.oProgramID, "ViewPosition");

    // Material
    oShaderStorage.oMaterial.oDiffuseUniformLocation = glGetUniformLocation(oShaderStorage.oProgramID, "Material.diffuse");
    oShaderStorage.oMaterial.oSpecularUniformLocation = glGetUniformLocation(oShaderStorage.oProgramID, "Material.specular");
    oShaderStorage.oMaterial.oShininessUniformLocation = glGetUniformLocation(oShaderStorage.oProgramID, "Material.shininess");

    // Set shader
    glUseProgram(oShaderStorage.oProgramID);
}

std::string Shader::GetName() {
	return std::string(oShaderStorage.oProgramName);
}

void Shader::SetProjection(const glm::mat4 aProjectionMatrix) {
    glUseProgram(oShaderStorage.oProgramID);

    glUniformMatrix4fv(oShaderStorage.oProjectionMatrixUniformLocation, 1, GL_FALSE, &aProjectionMatrix[0][0]);

    glUseProgram(0);
}

void Shader::SetCamera(Camera* aCamera) {
    // Set activated camera
    oCamera = aCamera;

    // Get transformation matrix
    glm::mat4 lViewMatrix = oCamera->GetTransformMatrix();
    glm::vec3 lViewPosition = oCamera->GetPosition();

    // Push matrix to vertex shader
    glUseProgram(oShaderStorage.oProgramID);

    glUniformMatrix4fv(oShaderStorage.oViewMatrixUniformLocation, 1, GL_FALSE, &lViewMatrix[0][0]);
    glUniform3f(oShaderStorage.oViewPositionUniformLocation, lViewPosition.x, lViewPosition.y, lViewPosition.z);

    if (aCamera != nullptr) {
        //UpdateCamera(aCamera);
        aCamera->AddListener(this);
    }
}

void Shader::AddLights(std::vector<Light*> aLights) {
    Light* lLight = aLights.back();
    if (lLight == nullptr) {
        std::cout << "Inserted light is nullptr." << std::endl;
        return;
    }

    lLight->AddListener(this);
    UpdateLights(aLights);
}

void Shader::UpdateCamera(const Camera* aCamera) {
    glm::mat4 lViewMatrix = aCamera->GetTransformMatrix();
    glm::mat4 lProjectionMatrix = aCamera->GetProjectionMatrix();

    glUseProgram(oShaderStorage.oProgramID);

    glUniformMatrix4fv(oShaderStorage.oViewMatrixUniformLocation, 1, GL_FALSE, &lViewMatrix[0][0]);
    glUniformMatrix4fv(oShaderStorage.oProjectionMatrixUniformLocation, 1, GL_FALSE, &lProjectionMatrix[0][0]);

    glUseProgram(0);
}

void Shader::UpdateLights(std::vector<Light*> aLights) {

    glUseProgram(oShaderStorage.oProgramID);

    for (GLuint i = 0; i < aLights.size(); i++) {
        LightType lLightType = aLights[i]->GetLightType();

        switch(lLightType) {
        case DIRECTION_LIGHT:
            glUniform1i(
                glGetUniformLocation(oShaderStorage.oProgramID, "NUMBER_OF_DIR_LIGHTS"),
                aLights.size()
            );
            UpdateDirectionLight(aLights[i], i);
            break;
        case POINT_LIGHT:
            glUniform1i(
                glGetUniformLocation(oShaderStorage.oProgramID, "NUMBER_OF_POINT_LIGHTS"),
                aLights.size()
            );
            UpdatePointLight(aLights[i], i);
            break;
        case SPOT_LIGHT:
            glUniform1i(
                glGetUniformLocation(oShaderStorage.oProgramID, "NUMBER_OF_SPOT_LIGHTS"),
                aLights.size()
            );
            UpdateSpotLight(aLights[i], i);
            break;
        default:
            std::cout << "Unknown light type during updating lights." << std::endl;
            break;
        }
    }

    glUseProgram(0);
}

void Shader::UpdateDirectionLight(Light* aLight, GLuint aIndex) {

    std::string lDirDirectionAttrName = std::string("dirLight[" + GLuintToStr(aIndex) + "].direction");
    glUniform3fv(
        glGetUniformLocation(oShaderStorage.oProgramID, lDirDirectionAttrName.c_str()),
        1,
        &(aLight->GetDirection())[0]
    );

    std::string lAmbientAttrName = std::string("dirLight[" + GLuintToStr(aIndex) + "].ambient");
    glUniform3fv(
        glGetUniformLocation(oShaderStorage.oProgramID, lAmbientAttrName.c_str()),
        1,
        &(aLight->GetAmbient())[0]
    );

    std::string lDiffuseAttrName = std::string("dirLight[" +  GLuintToStr(aIndex) + "].diffuse");
    glUniform3fv(
        glGetUniformLocation(oShaderStorage.oProgramID, lDiffuseAttrName.c_str()),
        1,
        &(aLight->GetDiffuse())[0]
    );

    std::string lSpecularAttrName = std::string("dirLight[" +  GLuintToStr(aIndex) + "].specular");
    glUniform3fv(
        glGetUniformLocation(oShaderStorage.oProgramID, lSpecularAttrName.c_str()),
        1,
        &(aLight->GetSpecular())[0]
    );
}

void Shader::UpdatePointLight(Light* aLight, GLuint aIndex) {

    std::string lPositionAttrName = std::string("pointLight[" +  GLuintToStr(aIndex) + "].position");
    glUniform3fv(
        glGetUniformLocation(oShaderStorage.oProgramID, lPositionAttrName.c_str()),
        1,
        &(aLight->GetPosition())[0]
    );

    std::string lConstantAttrName = std::string("pointLight[" +  GLuintToStr(aIndex) + "].constant");
    glUniform1f(
        glGetUniformLocation(oShaderStorage.oProgramID, lConstantAttrName.c_str()),
        aLight->GetConstant()
    );

    std::string lLinearAttrName = std::string("pointLight[" +  GLuintToStr(aIndex) + "].linear");
    glUniform1f(
        glGetUniformLocation(oShaderStorage.oProgramID, lLinearAttrName.c_str()),
        aLight->GetLinear()
    );

    std::string lQuadraticAttrName = std::string("pointLight[" +  GLuintToStr(aIndex) + "].quadratic");
    glUniform1f(
        glGetUniformLocation(oShaderStorage.oProgramID, lQuadraticAttrName.c_str()),
        aLight->GetQuadratic()
    );

    std::string lAmbientAttrName = std::string("pointLight[" +  GLuintToStr(aIndex) + "].ambient");
    glUniform3fv(
        glGetUniformLocation(oShaderStorage.oProgramID, lAmbientAttrName.c_str()),
        1,
        &(aLight->GetAmbient())[0]
    );

    std::string lDiffuseAttrName = std::string("pointLight[" +  GLuintToStr(aIndex) + "].diffuse");
    glUniform3fv(
        glGetUniformLocation(oShaderStorage.oProgramID, lDiffuseAttrName.c_str()),
        1,
        &(aLight->GetDiffuse())[0]
    );

    std::string lSpecularAttrName = std::string("pointLight[" +  GLuintToStr(aIndex) + "].specular");
    glUniform3fv(
        glGetUniformLocation(oShaderStorage.oProgramID, lSpecularAttrName.c_str()),
        1,
        &(aLight->GetSpecular())[0]
    );
}

void Shader::UpdateSpotLight(Light* aLight, GLuint aIndex) {

    std::string lPositionAttrName = std::string("spotLight[" +  GLuintToStr(aIndex) + "].position");
    glUniform3fv(
        glGetUniformLocation(oShaderStorage.oProgramID, lPositionAttrName.c_str()),
        1,
        &(aLight->GetPosition())[0]
    );

    std::string lDirectionAttrName = std::string("spotLight[" +  GLuintToStr(aIndex) + "].direction");
    glUniform3fv(
        glGetUniformLocation(oShaderStorage.oProgramID, lPositionAttrName.c_str()),
        1,
        &(aLight->GetPosition())[0]
    );

    std::string lCutOfftAttrName = std::string("spotLight[" +  GLuintToStr(aIndex) + "].cutOff");
    glUniform1f(
        glGetUniformLocation(oShaderStorage.oProgramID, lCutOfftAttrName.c_str()),
        aLight->GetCutOff()
    );

    std::string lOuterCutOffAttrName = std::string("spotLight[" +  GLuintToStr(aIndex) + "].outerCutOff");
    glUniform1f(
        glGetUniformLocation(oShaderStorage.oProgramID, lOuterCutOffAttrName.c_str()),
        aLight->GetOuterCutOff()
    );

    std::string lConstantAttrName = std::string("spotLight[" +  GLuintToStr(aIndex) + "].constant");
    glUniform1f(
        glGetUniformLocation(oShaderStorage.oProgramID, lConstantAttrName.c_str()),
        aLight->GetConstant()
    );

    std::string lLinearAttrName = std::string("spotLight[" +  GLuintToStr(aIndex) + "].linear");
    glUniform1f(
        glGetUniformLocation(oShaderStorage.oProgramID, lLinearAttrName.c_str()),
        aLight->GetLinear()
    );

    std::string lQuadraticAttrName = std::string("spotLight[" +  GLuintToStr(aIndex) + "].quadratic");
    glUniform1f(
        glGetUniformLocation(oShaderStorage.oProgramID, lQuadraticAttrName.c_str()),
        aLight->GetQuadratic()
    );

    std::string lAmbientAttrName = std::string("spotLight[" +  GLuintToStr(aIndex) + "].ambient");
    glUniform3fv(
        glGetUniformLocation(oShaderStorage.oProgramID, lAmbientAttrName.c_str()),
        1,
        &(aLight->GetAmbient())[0]
    );

    std::string lDiffuseAttrName = std::string("spotLight[" +  GLuintToStr(aIndex) + "].diffuse");
    glUniform3fv(
        glGetUniformLocation(oShaderStorage.oProgramID, lDiffuseAttrName.c_str()),
        1,
        &(aLight->GetDiffuse())[0]
    );

    std::string lSpecularAttrName = std::string("spotLight[" +  GLuintToStr(aIndex) + "].specular");
    glUniform3fv(
        glGetUniformLocation(oShaderStorage.oProgramID, lSpecularAttrName.c_str()),
        1,
        &(aLight->GetSpecular())[0]
    );
}

void Shader::Draw(const glm::mat4& aModelMatrix, const std::vector<Mesh>* aMeshes) const {
    // Bind appropriate textures
    GLuint diffuseNr = 1;
    GLuint specularNr = 1;
    GLuint lModelTexture = 1;

    glUseProgram(oShaderStorage.oProgramID);

    glUniformMatrix4fv(oShaderStorage.oModelMatrixUniformLocation, 1, GL_FALSE, &aModelMatrix[0][0]);

    for (GLuint i = 0; i < aMeshes->size(); i++) {
        Mesh mesh = aMeshes->at(i);

        for( GLuint j = 0; j < mesh.oTextures.size(); j++ )
        {
            glActiveTexture( GL_TEXTURE0 + j ); // Active proper texture unit before binding
            // Retrieve texture number (the N in diffuse_textureN)
            stringstream ss;
            string number;
            string name = mesh.oTextures[j].Type;

            if ( name == "texture_diffuse" ) {
                ss << diffuseNr++; // Transfer GLuint to stream
            }
            else if ( name == "texture_specular" ) {
                ss << specularNr++; // Transfer GLuint to stream
            }
            else if ( name == "ModelTexture" ) {
                ss << lModelTexture++;
            }

            number = ss.str( );
            // Now set the sampler to the correct texture unit
            glUniform1i( glGetUniformLocation( oShaderStorage.oProgramID, ( name + number ).c_str( ) ), i );
            //glUniform1i( glGetUniformLocation( oShaderStorage.oProgramID, "ModelTexture" ), 0 );
            // And finally bind the texture
            glBindTexture( GL_TEXTURE_2D, mesh.oTextures[j].Id );
        }

        // Draw mesh
        glBindVertexArray( mesh.GetVAO() );
        glDrawElements( GL_TRIANGLES, mesh.oIndices.size( ), GL_UNSIGNED_INT, 0 );
        glBindVertexArray( 0 );

        // Always good practice to set everything back to defaults once configured.
        for ( GLuint j = 0; j < mesh.oTextures.size( ); j++ )
        {
            glActiveTexture( GL_TEXTURE0 + j );
            glBindTexture( GL_TEXTURE_2D, 0 );
        }
    }

}

void Shader::Draw(const glm::mat4& aModelMatrix, const GLuint aVAO, const GLuint aVerticesAmount) const {
    glUseProgram(oShaderStorage.oProgramID);

    glUniformMatrix4fv(oShaderStorage.oModelMatrixUniformLocation, 1, GL_FALSE, &aModelMatrix[0][0]);

    glBindVertexArray(aVAO);

    glDrawArrays(GL_TRIANGLES, (GLint)0, aVerticesAmount);
}

void Shader::Draw(const glm::mat4& aModelMatrix, const GLuint aVAO, const GLuint aVerticesAmount, const GLuint aSpecularTexture, const GLuint aDiffuseTexture) const {
    glUseProgram(oShaderStorage.oProgramID);

    glUniformMatrix4fv(oShaderStorage.oModelMatrixUniformLocation, 1, GL_FALSE, &aModelMatrix[0][0]);

    //glUniform1i( glGetUniformLocation( lightingShader.Program, "material.diffuse" ), 0 );
    //glUniform1i( glGetUniformLocation( lightingShader.Program, "material.specular" ), 1 );

    glActiveTexture( GL_TEXTURE0 );
    glBindTexture( GL_TEXTURE_2D, aDiffuseTexture );

    glActiveTexture( GL_TEXTURE1 );
    glBindTexture( GL_TEXTURE_2D, aSpecularTexture );

    glBindVertexArray(aVAO);

    glDrawArrays(GL_TRIANGLES, (GLint)0, aVerticesAmount);
}

void Shader::DrawSkybox(const glm::mat4& aModelMatrix, const GLuint aTextureID, const GLuint aVAO, const int aVerticiesAmount) const {

    glUseProgram(oShaderStorage.oProgramID);

    glUniformMatrix4fv(oShaderStorage.oModelMatrixUniformLocation, 1, GL_FALSE, &aModelMatrix[0][0]);

    glDepthFunc( GL_LEQUAL );

    glBindVertexArray( aVAO );
    glBindTexture( GL_TEXTURE_CUBE_MAP, aTextureID );
    glDrawArrays( GL_TRIANGLES, 0, aVerticiesAmount );

    glBindVertexArray( 0 );

    glDepthFunc( GL_LESS );
}
