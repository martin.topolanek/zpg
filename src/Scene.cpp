#pragma once
#include "OpenGLIncludes.h"
#include "Includes.h"
#include "Scene.h"
#include "Shader.h"
#include "BaseObject.h"
#include "Window.h"

Scene Scene::oScene;

Scene::Scene() {

}

BaseObject* Scene::FindObject(BaseObject* aObject) {
    if (aObject == nullptr)
	{
		return nullptr;
	}
	else
	{
		for (int i = 0; i < (int)oObjects.size(); i++)
		{
			if (aObject == oObjects[i])
			{
				return aObject;
			}
		}
		return nullptr;
	}
}

void Scene::AddObject(BaseObject* aObject) {
    if (aObject == nullptr) {
        return;
    }
    if (FindObject(aObject) == nullptr) {
        oObjects.push_back(aObject);
        if (oShaders.size() > 0) {
            aObject->SetShadingProgram(oShaders[0]);
        }
    }
}

void Scene::AddObject(BaseObject* aObject, std::string aShaderName) {
    if (aObject == nullptr) {
        return;
    }
    if (dynamic_cast<Skybox*>(aObject)) {
        this->oSkybox = (Skybox*)aObject;
    }
    if (FindObject(aObject) == nullptr) {
        oObjects.push_back(aObject);

        if (oShaders.size() > 0) {
            aObject->SetShadingProgram(oShaders[0]);

            for (GLuint i = 0; i < oShaders.size(); i++) {
                if (oShaders[i]->GetName() == aShaderName) {
                    aObject->SetShadingProgram(oShaders[i]);
                }
            }
        }
    }
}

void Scene::AddLight(Light* aLight) {
    LightStorage* lLightStorage = LightStorage::GetLightStorageManager();
    LightType lLightType = aLight->GetLightType();

    lLightStorage->AddLight(aLight);

    for (GLuint i = 0; i < oShaders.size(); i++) {
        if (lLightType == DIRECTION_LIGHT) {
            oShaders[i]->AddLights(lLightStorage->GetDirectionLights());
        } else if (lLightType == SPOT_LIGHT) {
            oShaders[i]->AddLights(lLightStorage->GetSpotLights());
        } else if (lLightType == POINT_LIGHT) {
            oShaders[i]->AddLights(lLightStorage->GetPointLights());
        }
    }
}

Scene* Scene::GetScene() {
    return &oScene;
}

void Scene::CreateShader(const std::string aVertexShaderName, const std::string aFragmentShaderName, const std::string aProgramName) {
    Shader* tShader = new Shader(aVertexShaderName, aFragmentShaderName, aProgramName);

    oShaders.push_back(tShader);
}

const Shader* Scene::GetShader(const std::string aShaderName) {
    Shader* lShader = oShaders[0];

    for (unsigned int i = 0; i < oShaders.size(); i++) {
        if (oShaders[i]->GetName() == aShaderName) {
            lShader = oShaders[i];
            break;
        }
    }

    return lShader;
}

Skybox* Scene::GetSkybox() {
    return this->oSkybox;
}

void Scene::AddCamera(Camera* aCamera) {
    if (aCamera == nullptr) {
        std::cout << "Camera is nullptr!" << std::endl;
        return;
    }
    for (unsigned int i = 0; i < oCameras.size(); i++) {
        if (oCameras[i] == aCamera) {
            std::cout << "Inserted camera #" << i << " already exist!" << std::endl;
            return;
        }
    }

    for (unsigned int i = 0; i < oShaders.size(); i++) {
        oShaders[i]->SetCamera(aCamera);
    }

    oActiveCamera = aCamera;
    oCameras.push_back(aCamera);
}
/*
void Scene::AddLightStorage(LightStorage* aLightStorage) {

    this->oLightStorage = aLightStorage;

    for (unsigned int i = 0; i < oShaders.size(); i++) {
        oShaders[i]->AddLights(aLightStorage->GetDirectionLights(), DIRECTION_LIGHT);
        oShaders[i]->AddLights(aLightStorage->GetPointLights(), POINT_LIGHT);
        oShaders[i]->AddLights(aLightStorage->GetSpotLights(), SPOT_LIGHT);
    }

}*/

void Scene::Draw() {
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

	for (int i = 0; i < (int)oObjects.size(); i++)
	{
		oObjects[i]->Draw();
	}

    glfwSwapBuffers(Window::getWindow()->getGLFWWindow());
}

void Scene::ClockTick() {
    oActiveCamera->ClockTick();

    LightStorage* lLightStorage = LightStorage::GetLightStorageManager();
    for (GLuint i = 0; i < oShaders.size(); i++) {
        oShaders[i]->UpdateLights(lLightStorage->GetDirectionLights());
        oShaders[i]->UpdateLights(lLightStorage->GetPointLights());
        //oShaders[i]->UpdateLights(lLightStorage->GetSpotLights());
    }
}
