#include "Light.h"

Light::Light(LightType aLightType) {
    this->oPosition = glm::vec3(0.0f, 0.0f, 0.0f);
    this->oLightType = aLightType;
}

Light::Light(glm::vec3 aPosition, LightType aLightType) {
    this->oPosition = aPosition;
    this->oLightType = aLightType;

    // Set default values for every type of light
    this->oAmbient  = glm::vec3(0.05f, 0.05f, 0.05f);
    this->oDiffuse  = glm::vec3(0.8f, 0.8f, 0.8f);
    this->oSpecular = glm::vec3(1.0f, 1.0f, 1.0f);
}

LightType Light::GetLightType() {
    return this->oLightType;
}

void Light::AddListener(LightListener* aLightListener) {
    this->oListeners.push_back(aLightListener);
}

void Light::UpdateListeners() {
    for (GLuint i = 0; i < oListeners.size(); i++) {
        std::vector<Light*> lLights = std::vector<Light*>();

        if (this->GetLightType() == DIRECTION_LIGHT) {
            std::vector<Light*> lDirLights = LightStorage::GetLightStorageManager()->GetDirectionLights();
            lLights.insert(lLights.end(), lDirLights.begin(), lDirLights.end());
        } else if (this->GetLightType() == SPOT_LIGHT) {
            std::vector<Light*> lSpotLights = LightStorage::GetLightStorageManager()->GetSpotLights();
            lLights.insert(lLights.end(), lSpotLights.begin(), lSpotLights.end());
        } else if (this->GetLightType() == POINT_LIGHT) {
            std::vector<Light*> lPointLights = LightStorage::GetLightStorageManager()->GetPointLights();
            lLights.insert(lLights.end(), lPointLights.begin(), lPointLights.end());
        }

        oListeners[i]->UpdateLights(lLights);
    }
}

/* ALL LIGHT TYPES */
Light* Light::SetPosition(glm::vec3 aPosition) {
    this->oPosition = aPosition;

    return this;
}

Light* Light::SetAmbient(glm::vec3 aAmbient) {
    this->oAmbient = aAmbient;

    return this;
}

Light* Light::SetDiffuse(glm::vec3 aDiffuse) {
    this->oDiffuse = aDiffuse;

    return this;
}

Light* Light::SetSpecular(glm::vec3 aSpecular) {
    this->oSpecular = aSpecular;

    return this;
}

glm::vec3 Light::GetPosition() const {
    return oPosition;
}

glm::vec3 Light::GetAmbient() {
    return this->oAmbient;
}

glm::vec3 Light::GetDiffuse() {
    return this->oDiffuse;
}

glm::vec3 Light::GetSpecular() {
    return this->oSpecular;
}

/* DIRECTIONAL & SPOT LIGHT*/
Light* Light::SetDirection(glm::vec3 aDirection) {
    this->oDirection = aDirection;

    return this;
}

glm::vec3 Light::GetDirection() const {
    return this->oDirection;
}

/* SPOT & POINT LIGHT */
Light* Light::SetConstant(GLfloat aConstant) {
    this->oConstant = aConstant;

    return this;
}

Light* Light::SetLinear(GLfloat aLinear) {
    this->oLinear = aLinear;

    return this;
}

GLfloat Light::GetConstant() {
    return this->oConstant;
}

GLfloat Light::GetLinear() {
    return this->oLinear;
}

/* POINT LIGHT */
Light* Light::SetQuadratic(GLfloat aQuadratic) {
    this->oQuadratic = aQuadratic;

    return this;
}

Light* Light::SetCutOff(GLfloat aCutOff) {
    this->oCutOff = glm::cos(glm::radians( aCutOff ));

    return this;
}

Light* Light::SetOuterCutOff(GLfloat aOuterCutOff) {
    this->oOuterCutOff = glm::cos(glm::radians( aOuterCutOff ));

    return this;
}

GLfloat Light::GetQuadratic() {
    return this->oQuadratic;
}

GLfloat Light::GetCutOff() {
    return this->oCutOff;
}

GLfloat Light::GetOuterCutOff() {
    return this->oOuterCutOff;
}
