#include "Mesh.h"

Mesh::Mesh( std::vector<MeshVertex> aVertices, std::vector<GLuint> aIndices, std::vector<Texture> aTextures ) {
    this->oVertices = aVertices;
    this->oIndices = aIndices;
    this->oTextures = aTextures;

    SetupMesh();
}

void Mesh::SetupMesh() {
    // Create buffers/arrays
    // TO DO GetVAO() -- &this->VAO
    glGenVertexArrays( 1, &this->VAO );
    // TO DO GetVBO()
    glGenBuffers( 1, &this->VBO );
    // TO DO GetEBO()
    glGenBuffers( 1, &this->EBO );

    glBindVertexArray( this->VAO );
    // Load data into vertex buffers
    glBindBuffer( GL_ARRAY_BUFFER, this->VBO );
    // A great thing about structs is that their memory layout is sequential for all its items.
    // The effect is that we can simply pass a pointer to the struct and it translates perfectly to a glm::vec3/2 array which
    // again translates to 3/2 floats which translates to a byte array.
    // TO DO GetVertices
    glBufferData( GL_ARRAY_BUFFER, this->oVertices.size( ) * sizeof( MeshVertex ), &this->oVertices[0], GL_STATIC_DRAW );

    glBindBuffer( GL_ELEMENT_ARRAY_BUFFER, this->EBO );
    glBufferData( GL_ELEMENT_ARRAY_BUFFER, this->oIndices.size( ) * sizeof( GLuint ), &this->oIndices[0], GL_STATIC_DRAW );

    // Set the vertex attribute pointers
    // Vertex Positions
    glEnableVertexAttribArray( 0 );
    glVertexAttribPointer( 0, 3, GL_FLOAT, GL_FALSE, sizeof( MeshVertex ), ( GLvoid * )0 );
    // Vertex Normals
    glEnableVertexAttribArray( 1 );
    glVertexAttribPointer( 1, 3, GL_FLOAT, GL_FALSE, sizeof( MeshVertex ), ( GLvoid * )offsetof( MeshVertex, Normal ) );
    // Vertex Texture Coords
    glEnableVertexAttribArray(2);
    glVertexAttribPointer( 2, 2, GL_FLOAT, GL_FALSE, sizeof( MeshVertex ), ( GLvoid * )offsetof( MeshVertex, TexCoords ) );

    glBindVertexArray( 0 );
}

GLuint Mesh::GetVAO() {
    return this->VAO;
}

GLuint Mesh::GetVBO() {
    return this->VBO;
}

GLuint Mesh::GetEBO() {
    return this->EBO;
}
