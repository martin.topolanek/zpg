#include "Skybox.h"

Skybox::Skybox():BaseObject()
{
    std::vector<std::string> lFaces;
    lFaces.push_back(GetPath(TEXTURE_PATH, "skybox/left.png"));
    lFaces.push_back(GetPath(TEXTURE_PATH, "skybox/right.png"));
    lFaces.push_back(GetPath(TEXTURE_PATH, "skybox/up.png"));
    lFaces.push_back(GetPath(TEXTURE_PATH, "skybox/down.png"));
    lFaces.push_back(GetPath(TEXTURE_PATH, "skybox/front.png"));
    lFaces.push_back(GetPath(TEXTURE_PATH, "skybox/back.png"));

    this->oFaces = lFaces;
    this->oTextureManager = TextureManager::getTextureManager();

    this->oVerticiesSize = 36;

    this->SetupSkybox();
}

void Skybox::SetupSkybox() {
    GLfloat skyboxVertices[] = {
        // Positions
        -1.0f,  1.0f, -1.0f,
        -1.0f, -1.0f, -1.0f,
        1.0f, -1.0f, -1.0f,
        1.0f, -1.0f, -1.0f,
        1.0f,  1.0f, -1.0f,
        -1.0f,  1.0f, -1.0f,

        -1.0f, -1.0f,  1.0f,
        -1.0f, -1.0f, -1.0f,
        -1.0f,  1.0f, -1.0f,
        -1.0f,  1.0f, -1.0f,
        -1.0f,  1.0f,  1.0f,
        -1.0f, -1.0f,  1.0f,

        1.0f, -1.0f, -1.0f,
        1.0f, -1.0f,  1.0f,
        1.0f,  1.0f,  1.0f,
        1.0f,  1.0f,  1.0f,
        1.0f,  1.0f, -1.0f,
        1.0f, -1.0f, -1.0f,

        -1.0f, -1.0f,  1.0f,
        -1.0f,  1.0f,  1.0f,
        1.0f,  1.0f,  1.0f,
        1.0f,  1.0f,  1.0f,
        1.0f, -1.0f,  1.0f,
        -1.0f, -1.0f,  1.0f,

        -1.0f,  1.0f, -1.0f,
        1.0f,  1.0f, -1.0f,
        1.0f,  1.0f,  1.0f,
        1.0f,  1.0f,  1.0f,
        -1.0f,  1.0f,  1.0f,
        -1.0f,  1.0f, -1.0f,

        -1.0f, -1.0f, -1.0f,
        -1.0f, -1.0f,  1.0f,
        1.0f, -1.0f, -1.0f,
        1.0f, -1.0f, -1.0f,
        -1.0f, -1.0f,  1.0f,
        1.0f, -1.0f,  1.0f
    };

    glGenVertexArrays( 1, &oVAO );
    glGenBuffers( 1, &oVBO );
    glBindVertexArray( oVAO );
    glBindBuffer( GL_ARRAY_BUFFER, oVBO );
    glBufferData( GL_ARRAY_BUFFER, sizeof( skyboxVertices ), &skyboxVertices, GL_STATIC_DRAW );
    glEnableVertexAttribArray( 0 );
    glVertexAttribPointer( 0, 3, GL_FLOAT, GL_FALSE, 3 * sizeof( GLfloat ), ( GLvoid * ) 0 );
    glBindVertexArray(0);

    // Load textures

    oTextureManager->CreateCubeTexture(this->oFaces, "skybox");
    oTextureID = oTextureManager->GetTexture("skybox");
}

void Skybox::Draw() const {
    oShaderProgram->DrawSkybox(GetTransformationMatrix(), oTextureID, oVAO, oVerticiesSize);
}

void Skybox::AttachCamera(Camera* aCamera) {
    if (aCamera == nullptr) {
        return;
    }

    this->oCamera = aCamera;
    aCamera->AddListener(this);
}

void Skybox::UpdateCamera(const Camera* aCamera) {
    SetPosition(aCamera->GetPosition());
}

