#include "AssimpModel.h"

AssimpModel::AssimpModel(std::string aPath) {
    this->oAreExternalTextures = false;
    this->LoadModel(aPath);
}

AssimpModel::AssimpModel(std::string aPath, std::vector<std::string> aTexturePaths) {
    this->oAreExternalTextures = true;
    this->oTexturePaths = aTexturePaths;
    this->LoadModel(aPath);
}


void AssimpModel::LoadModel(std::string aPath) {
    Assimp::Importer lImporter;
    const aiScene *lScene = lImporter.ReadFile(aPath, aiProcess_Triangulate | aiProcess_FlipUVs);

    if (!lScene || lScene->mFlags == AI_SCENE_FLAGS_INCOMPLETE || !lScene->mRootNode) {
        std::cout << "Error::Assimp::" << lImporter.GetErrorString() << std::endl;

        return;
    }

    this->oDirectory = aPath.substr(0, aPath.find_last_of( '/' ));

    this->processNode(lScene->mRootNode, lScene);

}

/*
* Get vector of all meshes for shader to drawing them out
*/
std::vector<Mesh> AssimpModel::GetMeshes() {
    return this->oMeshes;
}

void AssimpModel::processNode(aiNode *aNode, const aiScene *aScene) {
    for (GLuint i = 0; i < aNode->mNumMeshes; i++) {
        aiMesh *lMesh = aScene->mMeshes[aNode->mMeshes[i]];

        this->oMeshes.push_back( this->processMesh(lMesh, aScene) );
    }

    for (GLuint i = 0; i < aNode->mNumChildren; i++) {
        this->processNode(aNode->mChildren[i], aScene);
    }
}

Mesh AssimpModel::processMesh( aiMesh *aMesh, const aiScene *aScene) {
    std::vector<MeshVertex> lVertices;
    std::vector<GLuint> lIndices;
    std::vector<Texture> lTextures;

    for (GLuint i = 0; i < aMesh->mNumVertices; i++) {
        MeshVertex lVertex;
        glm::vec3 lVector;

        // Positions
        lVector.x = aMesh->mVertices[i].x;
        lVector.y = aMesh->mVertices[i].y;
        lVector.z = aMesh->mVertices[i].z;
        lVertex.Position = lVector;

        // Normals
        lVector.x = aMesh->mNormals[i].x;
        lVector.y = aMesh->mNormals[i].y;
        lVector.z = aMesh->mNormals[i].z;

        // Texture coordinates
        if (aMesh->mTextureCoords[0]) {
            glm::vec2 lVec;

            lVec.x = aMesh->mTextureCoords[0][i].x;
            lVec.y = aMesh->mTextureCoords[0][i].y;
            lVertex.TexCoords = lVec;
        }
        else {
            lVertex.TexCoords = glm::vec2(0.0f, 0.0f);
        }

        lVertices.push_back(lVertex);
    }

    for (GLuint i = 0; i < aMesh->mNumFaces; i++) {
        aiFace lFace = aMesh->mFaces[i];

        for (GLuint j = 0; j < lFace.mNumIndices; j++) {
            lIndices.push_back(lFace.mIndices[j]);
        }
    }
    if (this->oAreExternalTextures == true) {
        for (GLuint i = 0; i < oTexturePaths.size(); i++) {
            std::string lPath = oTexturePaths[i];
            TextureManager::getTextureManager()->CreateTexture(lPath);

            Texture lTexture;
            lTexture.Id = TextureManager::getTextureManager()->GetTexture(lPath);
            lTexture.Path = lPath;
            lTexture.Type = "ModelTexture";

            lTextures.push_back(lTexture);
        }
    }
    else {
        if (aMesh->mMaterialIndex >= 0) {
            aiMaterial* lMaterial = aScene->mMaterials[aMesh->mMaterialIndex];

            std::vector<Texture> lDiffuseMaps = this->LoadMaterialTextures( lMaterial, aiTextureType_DIFFUSE, "texture_diffuse" );
            lTextures.insert(lTextures.end(), lDiffuseMaps.begin(), lDiffuseMaps.end());

            std::vector<Texture> lSpecularMaps = this->LoadMaterialTextures( lMaterial, aiTextureType_SPECULAR, "texture_specular" );
            lTextures.insert(lTextures.end(), lSpecularMaps.begin(), lSpecularMaps.end());
        }
    }

    return Mesh(lVertices, lIndices, lTextures);
}

std::vector<Texture> AssimpModel::LoadMaterialTextures(aiMaterial *aMaterial, aiTextureType aType, std::string aTypeName) {
    std::vector<Texture> lTextures;

    for (GLuint i = 0; i < aMaterial->GetTextureCount(aType); i++) {
        aiString lStr;
        aMaterial->GetTexture(aType, i, &lStr);

        GLboolean lSkip = false;

        for (GLuint j = 0; j < oTexturesLoaded.size(); j++) {
            if (oTexturesLoaded[j].Path == lStr) {
                lTextures.push_back(oTexturesLoaded[j]);
                lSkip = true;

                break;
            }
        }

        if (!lSkip) {
            Texture lTexture;
            lTexture.Id = TextureFromFile(lStr.C_Str(), this->oDirectory);
            lTexture.Type = aTypeName;
            lTexture.Path = lStr;

            lTextures.push_back(lTexture);

            this->oTexturesLoaded.push_back(lTexture);
        }
    }

    return lTextures;
}


GLint AssimpModel::TextureFromFile(const char* aPath, std::string aDirectory) {
    std::string lFileName = std::string(aPath);
    lFileName = aDirectory + '/' + lFileName;
    GLuint lTextureID;
    glGenTextures(1, &lTextureID);

    int width, height;

    unsigned char* lImage = SOIL_load_image(lFileName.c_str(), &width, &height, 0, SOIL_LOAD_RGB);

    glBindTexture(GL_TEXTURE_2D, lTextureID);
    glTexImage2D(GL_TEXTURE_2D, 0, GL_RGB, width, height, 0, GL_RGB, GL_UNSIGNED_BYTE, lImage);
    glGenerateMipmap(GL_TEXTURE_2D);

    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR_MIPMAP_LINEAR);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
    glBindTexture(GL_TEXTURE_2D, 0);
    SOIL_free_image_data(lImage);

    return lTextureID;
}
