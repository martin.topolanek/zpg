#include "ModelManager.h"

ModelManager ModelManager::mModelManager;

ModelManager::ModelManager()
{
    //ctor
}

ModelManager::~ModelManager()
{
    //dtor
}

ModelManager* ModelManager::GetModelManager() {
    return &mModelManager;
}

void ModelManager::CreateAssimpModel(std::string aModelFileName) {

    if (oAssimpModels.count(aModelFileName) > 0) {
        std::cout << "Object called '" << aModelFileName << "' already exist.";
        return;
    }

    AssimpModel* model = new AssimpModel(aModelFileName);

    oAssimpModels[aModelFileName] = model;
}

void ModelManager::CreateAssimpModel(std::string aModelFileName, std::vector<std::string> aTexturesPath) {

    if (oAssimpModels.count(aModelFileName) > 0) {
        std::cout << "Object called '" << aModelFileName << "' already exist.";
        return;
    }

    AssimpModel* model = new AssimpModel(aModelFileName, aTexturesPath);

    oAssimpModels[aModelFileName] = model;
}

void ModelManager::CreateRawModel(std::string aModelId, std::vector<Vertex> aVertices) {

    if (oRawModels.count(aModelId) > 0) {
        std::cout << "Object called '" << aModelId << "' already exist.";
        return;
    }

    RawModel* model = new RawModel(aVertices);

    oRawModels[aModelId] = model;
}

RawModel* ModelManager::GetRawModel(std::string aModelID) {
    return oRawModels[aModelID];
}

AssimpModel* ModelManager::GetAssimpModel(std::string aModelID) {
    return oAssimpModels[aModelID];
}


std::vector<Mesh> ModelManager::GetMeshes(std::string aModelID) {

    if (oAssimpModels.count(aModelID)) {
        return oAssimpModels[aModelID]->GetMeshes();
    }
    return std::vector<Mesh>();
}

