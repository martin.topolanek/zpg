#define GLEW_STATIC
#include <stdio.h>
//Include GLEW
#include <GL/glew.h>
//Include GLFW
#include <GLFW/glfw3.h>
#include <GL/gl.h>
#include <glm/gtx/transform.hpp>
#include "Paths.h"
#include <cstddef>
#include <cstdio>
#include "Application.h"
#include "Window.h"
#include "Scene.h"
#include "Object.h"
#include "Shader.h"
#include "ModelManager.h"
#include "Skybox.h"
#include "AssimpObject.h"
#include "RawObject.h"

Application::Application()
{
    //ctor
}

Application* Application::getInstance() {
    if (this->app == NULL) {
        this->app = new Application();
    }
    return this->app;
}

void Application::CreateShaders() {
    Scene* tScene = Scene::GetScene();

    tScene->CreateShader(GetPath(SHADER_PATH, "LightingShader.vertexshader"), GetPath(SHADER_PATH, "LightingShader.fragmentshader"), "Lighting Shader");
    tScene->CreateShader(GetPath(SHADER_PATH, "Skybox.vertexshader"), GetPath(SHADER_PATH, "Skybox.fragmentshader"), "Skybox shader");
}

void Application::CreateObjects() {

    Scene* tScene = Scene::GetScene();

    /* Terrain */
    std::string lTerrainModelPath = GetPath(MODEL_PATH, "plane.obj");
    std::string lTerrainTexturePath = GetPath(MODEL_PATH, "grass.jpg");

    std::vector<std::string> lTerrainTextures;
    lTerrainTextures.push_back(lTerrainTexturePath);
    ModelManager::GetModelManager()->CreateAssimpModel(lTerrainModelPath, lTerrainTextures);

    AssimpObject* lTerrainAssimp = new AssimpObject(lTerrainModelPath);
    lTerrainAssimp->SetPosition(glm::vec3(0.0,0.0,0.0));
    lTerrainAssimp->SetScale(20.0f);

    Skybox* lSkybox = new Skybox();
    lSkybox->SetPosition(glm::vec3(0, 1, 5));
    lSkybox->SetScale(10.0f);

    tScene->AddObject(lSkybox, "Skybox shader");

    /* House */
    std::string lHouseTexturePath = GetPath(MODEL_PATH, "test.png");
    std::string lHouseModelPath = GetPath(MODEL_PATH, "test.obj");

    std::vector<std::string> lTestTextures;
    lTestTextures.push_back(lHouseTexturePath);
    ModelManager::GetModelManager()->CreateAssimpModel(lHouseModelPath, lTestTextures);

    AssimpObject* lHouse = new AssimpObject(lHouseModelPath);
    lHouse->SetPosition(glm::vec3(0.0,0.0,0.0));


    /* Zombie */
    std::string lZombieModelPath = GetPath(MODEL_PATH, "zombie/zombie.fbx");
    ModelManager::GetModelManager()->CreateAssimpModel(lZombieModelPath);

    AssimpObject* lZombie = new AssimpObject(lZombieModelPath);
    lZombie->SetPosition(glm::vec3(0.0,0.0,0.0));
    lZombie->SetScale(0.01f);


    /* Nanosuit */
    std::string lNanosuitModelPath = GetPath(MODEL_PATH, "nanosuit/nanosuit.obj");
    ModelManager::GetModelManager()->CreateAssimpModel(lNanosuitModelPath);

    AssimpObject* lNanoSuit = new AssimpObject(lNanosuitModelPath);
    lNanoSuit->SetPosition(glm::vec3(2.0, 0.0, 0.0));
    lNanoSuit->RotateY(-1.0f);
    lNanoSuit->SetScale(0.1f);


    /* Adding objects to scene */
    tScene->AddObject(lTerrainAssimp);
    tScene->AddObject(lNanoSuit);
    tScene->AddObject(lZombie);
    tScene->AddObject(lHouse);
}

void KeyCallback(GLFWwindow* aWindow, int aKey, int aScancode, int aAction, int aMods) {

    // Manage camera
    if (aKey == GLFW_KEY_W || GLFW_KEY_A || GLFW_KEY_S || GLFW_KEY_D) {
        if (aAction == GLFW_PRESS)
            KeyboardManager::GetKeyboardManager()->KeyPressed(aKey);
        if (aAction == GLFW_RELEASE) {
            KeyboardManager::GetKeyboardManager()->KeyReleased(aKey);
        }
    }

    // Manage light
    if (aKey == GLFW_KEY_UP || GLFW_KEY_DOWN) {
        if (aAction == GLFW_PRESS) {
            KeyboardManager::GetKeyboardManager()->KeyPressed(aKey);
        }
        if (aAction == GLFW_RELEASE) {
            KeyboardManager::GetKeyboardManager()->KeyReleased(aKey);
        }
    }
}

void MouseCallback(GLFWwindow* aWindow, double aXPos, double aYPos) {
    MouseManager::GetMouseManager()->MouseMoved(aXPos, aYPos);
}

void ScrollCallback(GLFWwindow* aWindow, double aXOffset, double aYOffset) {
    MouseManager::GetMouseManager()->MouseScrolled(aYOffset);
}

void PeriodUpdate() {
    Scene* lScene = Scene::GetScene();
    lScene->ClockTick();
}

int Application::run() {

    if (!glfwInit()) {
        fprintf(stderr, "ERROR: could not start GLFW3\n");
        return 1;
    }

    Scene*      lScene      = Scene::GetScene();
    Window*     appWindow   = Window::getWindow();
    GLFWwindow* window      = appWindow->getGLFWWindow();

    glfwSetInputMode(window, GLFW_CURSOR, GLFW_CURSOR_DISABLED);
    glEnable(GL_DEPTH_TEST);

    CreateShaders();
	CreateObjects();

    Camera lCamera = Camera(glm::vec3(0, 1, 5), glm::vec3(0, 1, 0), -90, 0);
    lScene->AddCamera(&lCamera);

    lScene->GetSkybox()->AttachCamera(&lCamera);


    /* LIGHTS */
    Light* lPointLight1 = new Light(glm::vec3(0.0, 0.0, 0.0), POINT_LIGHT);
    lPointLight1->SetConstant(1.0f)->SetLinear(0.0014f)->SetQuadratic(0.000007f);

    Light* lDirLight1 = new Light(DIRECTION_LIGHT);
    lDirLight1->SetDirection(glm::vec3(1.0f, 1.0f, 1.0f))->SetAmbient(glm::vec3(0.2f, 0.2f, 0.2f))->SetDiffuse(glm::vec3(0.4f, 0.4f, 0.4f))->SetSpecular(glm::vec3(0.5f, 0.5f, 0.5f));

    lScene->AddLight(lPointLight1);
    lScene->AddLight(lDirLight1);

	do {
        // Draw objects
        lScene->Draw();

        glfwSetScrollCallback(window, ScrollCallback);
        glfwSetCursorPosCallback(window, MouseCallback);
        glfwSetKeyCallback(window, KeyCallback);

        PeriodUpdate();

        glfwPollEvents();
    }
    while( glfwGetKey(window, GLFW_KEY_ESCAPE ) != GLFW_PRESS && glfwWindowShouldClose(window) == 0 );

	exit(EXIT_SUCCESS);

    return 0;
}
