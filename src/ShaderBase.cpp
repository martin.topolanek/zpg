#pragma once
#include "ShaderBase.h"

bool DebugShader(GLuint aShaderID, std::string aShaderName) {
	GLint status;
	glGetShaderiv(aShaderID, GL_COMPILE_STATUS, &status);

	if (status == GL_FALSE) {
		GLint infoLogLength;
		glGetShaderiv(aShaderID, GL_INFO_LOG_LENGTH, &infoLogLength);

		GLchar* strInfoLog = new GLchar[infoLogLength + 1];
		glGetShaderInfoLog(aShaderID, infoLogLength, NULL, strInfoLog);

        std::cout << "Compile failure in " << aShaderName << " : " << std::endl;
        std::cout << strInfoLog << std::endl;

		delete[] strInfoLog;
		return false;
	}

	return true;
}

void DebugShader(GLuint aShaderID) {
	DebugShader(aShaderID, "unknown");
}

bool DebugProgram(GLuint aProgramID, std::string aProgramName) {
	GLint status;
	glGetShaderiv(aProgramID, GL_LINK_STATUS, &status);

	if (status == GL_FALSE) {
		GLint infoLogLength;
		glGetProgramiv(aProgramID, GL_INFO_LOG_LENGTH, &infoLogLength);

		GLchar *strInfoLog = new GLchar[infoLogLength + 1];
		glGetProgramInfoLog(aProgramID, infoLogLength, NULL, strInfoLog);

        std::cout << "Linker failure while linking program " << aProgramName << " :" << std::endl;
        std::cout << strInfoLog << std::endl;

		delete[] strInfoLog;
		return false;
	}

	return true;
}
