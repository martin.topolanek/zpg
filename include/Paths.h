#pragma once
#define SHADER_PATH "shaders/"
#define MODEL_PATH "models/"
#define TEXTURE_PATH "textures/"

#include <stdlib.h>
#include <iostream>
#include <fstream>
#include <stdio.h>
#include <string.h>
#include <string>
#include <sstream>

static std::string GetPath(std::string aDirectory, std::string aFileName) {
    std::stringstream ss;

    ss << aDirectory << aFileName;

    return ss.str();
}
