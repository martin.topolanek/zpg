#pragma once
#include "Includes.h"
#include "Model.h"
#include "BaseObject.h"
#include "AssimpModel.h"
#include "RawModel.h"

class ModelManager
{
    public:
        ModelManager();
        virtual ~ModelManager();

        static ModelManager* GetModelManager();

        RawModel* GetRawModel(std::string aModelID);
        AssimpModel* GetAssimpModel(std::string aModelID);
        void CreateAssimpModel(std::string aModelFileName);
        void CreateAssimpModel(std::string aModelFileName, std::vector<std::string> aTexturesPath);
        void CreateRawModel(std::string aModelId, std::vector<Vertex> aVertices);
        int GetModelVerticesCount(std::string aModelID);
        std::vector<Mesh> GetMeshes(std::string aModelID);
        bool IsSpecularTexture(std::string aModelID);

    private:
        static ModelManager mModelManager;

        std::map<std::string, RawModel*> oRawModels;
        std::map<std::string, AssimpModel*> oAssimpModels;
};
