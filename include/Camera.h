#pragma once
#include "OpenGLIncludes.h"
#include "Includes.h"
#include "CameraListener.h"
#include "KeyboardListener.h"
#include "MouseListener.h"
#include "Window.h"

class Camera : KeyboardListener, MouseListener
{
    public:
        Camera(glm::vec3 aEye, glm::vec3 aUp, float aFi, float aPsi);
        ~Camera();

        glm::mat4 GetTransformMatrix() const;
        glm::mat4 GetProjectionMatrix() const;

        glm::vec3 GetPosition() const;
        glm::vec3 GetDirection() const;
        glm::vec3 GetDirection(float oFi, float oPsi) const;

        void RotateHorizontal(float aAngle);
        void RotateVertical(float aAngle);
        void MoveForward(float aShift);
        void MoveToSide(float aShift);

        void AddListener(CameraListener* aListener);
        void RemoveListener(CameraListener* aListener);
        void UpdateListeners();

        void Update(int aKey, int aAction);
        void ClockTick();

        void KeyPressed(int aKey) override;
        void KeyReleased(int aKey) override;
        void MouseMoved(double aPrevXPos, double aXPos, double aPrevYPos, double aYPos);
        void MouseScrolled(double aYOffset);

    private:
        void UpdateListeners() const;

        // ViewModel
        glm::vec3 oEye;
        glm::vec3 oUp;

        // ProjectionModel
        glm::mat4 oProjectionMatrix;
        float oProjectionAngle = 45.0f;

        float oFi;
        float oPsi;

        std::vector<CameraListener*> oListeners;

        float oForwardBackwardSpeed = 0;
        float oLeftRightSpeed = 0;
        float oMouseSpeed = 0.1;
};
