#pragma once
#include "BaseObject.h"
#include "TextureManager.h"
#include "OpenGLIncludes.h"
#include "Includes.h"
#include "CameraListener.h"
#include "Paths.h"

class Skybox : public BaseObject, public CameraListener
{
    public:
        Skybox();
        void AttachCamera(Camera* aCamera);
        void UpdateCamera(const Camera* aCamera);
        void Draw() const;

    private:
        TextureManager* oTextureManager;
        Camera* oCamera;

        int oVerticiesSize;
        GLuint oTextureID;
        std::vector<std::string> oFaces;

        void SetupSkybox();
};
