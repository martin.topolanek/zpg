#pragma once
#include "Includes.h"
#include "Types.h"
#include "OpenGLIncludes.h"

class Mesh
{
public:
    std::vector<MeshVertex> oVertices;
    std::vector<GLuint> oIndices;
    std::vector<Texture> oTextures;

    Mesh( std::vector<MeshVertex> aVertices, std::vector<GLuint> aIndices, std::vector<Texture> aTextures );

    GLuint GetVAO();
    GLuint GetVBO();
    GLuint GetEBO();

private:
    GLuint VAO, VBO, EBO;

    void SetupMesh();
};
