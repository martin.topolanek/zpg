#pragma once
#include "Light.h"

class Light;
class LightListener {
public:
    virtual void UpdateLights(std::vector<Light*> aLights) = 0;
};
