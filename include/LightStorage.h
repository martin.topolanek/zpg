#pragma once
#include "Includes.h"
#include "Light.h"
#include "LightType.h"

#define MAX_DIRECTION_LIGHTS 1
#define MAX_SPOT_LIGHT 4
#define MAX_POINT_LIGHT 5

class LightStorage
{
    public:
        LightStorage();
        static LightStorage* GetLightStorageManager();

        void AddLight(Light* aLight);
        void RemoveLight(Light* aLight);

        void CreateDirectionalLight(Light* aLight);
        void CreatePointLight(Light* aLight);
        void CreateSpotLight(Light* aLight);

        std::vector<Light*> GetDirectionLights();
        std::vector<Light*> GetPointLights();
        std::vector<Light*> GetSpotLights();

    private:
        std::map<LightType, std::vector<Light*>> oLightStorage;

        static LightStorage oLightStorageManager;
};
