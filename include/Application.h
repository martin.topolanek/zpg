#ifndef APPLICATION_H
#define APPLICATION_H


class Application
{
    public:
        Application();
        Application* getInstance();
        void CreateShaders();
        void CreateObjects();
        int run();

    protected:

    private:
        Application* app;
};

#endif // APPLICATION_H
