#pragma once
#include "Model.h"
#include "Mesh.h"
#include <SOIL/SOIL.h>
#include <Assimp/Importer.hpp>
#include <Assimp/scene.h>
#include <Assimp/postprocess.h>

class AssimpModel : public Model
{
    public:
        AssimpModel(std::string aPath);
        AssimpModel(std::string aPath, std::vector<std::string> aTexturePaths);
        std::vector<Mesh> GetMeshes();

    private:
        bool oAreExternalTextures;
        std::vector<std::string> oTexturePaths;
        std::vector<Mesh> oMeshes;
        std::string oDirectory;
        std::vector<Texture> oTexturesLoaded;

        void LoadModel(std::string aPath);
        void processNode(aiNode *aNode, const aiScene *aScene);
        Mesh processMesh( aiMesh *aMesh, const aiScene *aScene);
        std::vector<Texture> LoadMaterialTextures(aiMaterial *aMaterial, aiTextureType aType, std::string aTypeName);
        GLint TextureFromFile(const char* aPath, std::string aDirectory);
};
