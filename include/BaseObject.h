#pragma once
#include "OpenGLIncludes.h"
#include "Includes.h"
#include "Types.h"
#include "Shader.h"

class BaseObject
{
    public:
        BaseObject();
        virtual ~BaseObject();

        glm::mat4 GetTransformationMatrix() const;

        void Rotate(const glm::vec3& aRotationVector, float aAngle);
        void RotateX(float aAngle);
        void RotateY(float aAngle);
        void RotateZ(float aAngle);
        void SetScale(float aConst);

        void SetPosition(const glm::vec3& aPosition);

        virtual void Draw() const = 0;
        void SetShadingProgram(const Shader* aShadingProgram);

    protected:
        // Vertexs
        glm::vec3 oPosition;
        glm::mat4 oRotationMatrix;
        glm::mat4 oScaleMatrix;

        // Shader
        const Shader* oShaderProgram;
        GLuint oVAO, oVBO;
};
