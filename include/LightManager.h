#pragma once

class LightManager
{
    public:
        LightManager();

        static LightManager* GetLightManager();

    private:
        static LightManager oLightManager;
};
