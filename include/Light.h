#pragma once
#include "Includes.h"
#include "OpenGLIncludes.h"
#include "LightListener.h"
#include "LightType.h"
#include "LightStorage.h"
class Light
{
    public:
        Light(LightType aLightType);
        Light(glm::vec3 aPosition, LightType aLightType);

        LightType GetLightType();

        void AddListener(LightListener* aLightListener);
        void UpdateListeners();

        Light* SetPosition(glm::vec3 aPosition);
        glm::vec3 GetPosition() const;

        Light* SetAmbient(glm::vec3 aAmbient);
        Light* SetDiffuse(glm::vec3 aDiffuse);
        Light* SetSpecular(glm::vec3 aSpecular);
        glm::vec3 GetAmbient();
        glm::vec3 GetDiffuse();
        glm::vec3 GetSpecular();

        Light* SetDirection(glm::vec3 aDirection);
        glm::vec3 GetDirection() const;

        Light* SetConstant(GLfloat aConstant);
        Light* SetLinear(GLfloat aLinear);
        GLfloat GetConstant();
        GLfloat GetLinear();

        Light* SetQuadratic(GLfloat aQuadratic);
        Light* SetCutOff(GLfloat aCutOff);
        Light* SetOuterCutOff(GLfloat aOuterCutOff);
        GLfloat GetQuadratic();
        GLfloat GetCutOff();
        GLfloat GetOuterCutOff();

    private:
        std::vector<LightListener*> oListeners;

        LightType oLightType;
        glm::vec3 oPosition;
        glm::vec3 oAmbient;
        glm::vec3 oDiffuse;
        glm::vec3 oSpecular;

        glm::vec3 oDirection;

        GLfloat oConstant;
        GLfloat oLinear;

        GLfloat oQuadratic;
        GLfloat oCutOff;
        GLfloat oOuterCutOff;
};
