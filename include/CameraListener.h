#pragma once
#include "Camera.h"
class Camera;
class CameraListener {
public:
    virtual void UpdateCamera(const Camera* aCamera) = 0;
};
