#ifndef WINDOW_H
#define WINDOW_H
#include <cstddef>
#include <cstdio>
#include <stdio.h>

//Include GLEW
#include <GL/glew.h>
//Include GLFW
#include <GLFW/glfw3.h>
#include <GL/gl.h>


class Window
{
    public:
        static void version_info();
        Window();
        static Window* getWindow();
        GLFWwindow* getGLFWWindow();
        int shouldClose();
        void InitCursors();

        int GetWidth();
        int GetHeight();

        float GetXPos();
        float GetYPos();

    protected:

    private:
        GLFWwindow* oGLFWWindow;
        static Window* oWindow;
        double oXPos, oYPos;

        int width, height;
};

#endif // WINDOW_H
