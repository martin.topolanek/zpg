#pragma once
#include "Includes.h"
#include "ShaderBase.h"
#include "ShaderLoader.h"
#include "Window.h"
#include "Camera.h"
#include "Light.h"
#include "LightType.h"
#include "LightListener.h"
#include "LightStorage.h"
#include "Mesh.h"

class Shader : public ShaderLoader, public CameraListener, public LightListener
{
    public:
        Shader(std::string aVertexShaderFileName, std::string aFragmentShaderFileName, std::string aShaderProgramName, int aMaxLights = 10);
        ~Shader();

        bool IsValid();

        void Draw(const glm::mat4& aModelMatrix, const std::vector<Mesh>* aMeshes) const;
        void Draw(const glm::mat4& aModelMatrix, const GLuint aVAO, const GLuint aVerticesAmount) const;
        void Draw(const glm::mat4& aModelMatrix, const GLuint aVAO, const GLuint aVerticesAmount, const GLuint aSpecularTexture, const GLuint aDiffuseTexture) const;
        void DrawSkybox(const glm::mat4& aModelMatrix, const GLuint aTextureID, const GLuint aVAO, const int aVerticiesAmount) const;

        std::string GetName();

        void SetProjection(const glm::mat4 aProjectionMatrix);

        void SetCamera(Camera* aCamera);
        void AddLights(std::vector<Light*> aLights);
        void RemoveLight();

        void UpdateCamera(const Camera* aCamera);
        void UpdateLights(std::vector<Light*> aLights);

        void UpdateDirectionLight(Light* aLight, GLuint aIndex);
        void UpdatePointLight(Light* aLight, GLuint aIndex);
        void UpdateSpotLight(Light* aLight, GLuint aIndex);

    private:
        void RefreshUsedParts();

        ShaderStorage oShaderStorage;
        bool oIsValid;

        Camera* oCamera;
        std::map<LightType, std::vector<Light*>> oLights;
};
