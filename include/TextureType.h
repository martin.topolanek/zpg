#pragma once

enum TextureType {
    DIFFUSE_TEXTURE,
    SPECULAR_TEXTURE
};
