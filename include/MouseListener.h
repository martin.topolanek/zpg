#pragma once
#include "MouseManager.h"

class MouseListener
{
    public:
        MouseListener();
        ~MouseListener();

        virtual void MouseMoved(double aPrevXPos, double aXPos, double aPrevYPos, double aYPos);
        virtual void MouseScrolled(double aYOffset);
};
