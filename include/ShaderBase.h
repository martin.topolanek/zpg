#pragma once
#include "Includes.h"
#include "OpenGLIncludes.h"

bool DebugShader(GLuint aShaderID, std::string aShaderName);
void DebugShader(GLuint aShaderID);
bool DebugProgram(GLuint aProgramID, std::string aProgramName);

struct Material {
    GLuint oDiffuseUniformLocation;
    GLuint oSpecularUniformLocation;
    GLuint oShininessUniformLocation;
};

struct ShaderStorage {
    std::string oProgramName;
    std::string oVertexShaderFileName, oFragmentShaderFileName;

    const GLchar *oVertexShader, *oFragmentShader;
    GLuint oVertexShaderID, oFragmentShaderID;
    GLuint oProgramID;

    // Base
    GLuint oModelMatrixUniformLocation;
    GLuint oViewMatrixUniformLocation;
    GLuint oProjectionMatrixUniformLocation;
    GLuint oViewPositionUniformLocation;

    // Material
    Material oMaterial;

};
