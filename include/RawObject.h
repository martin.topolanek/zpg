#pragma once
#include "Object.h"
#include "RawModel.h"
#include "ModelManager.h"

class RawObject : public Object
{
    public:
        RawObject(std::string aModelID);

    private:
        RawModel* oModel;
        GLuint oVerticesAmount;

        virtual void Draw() const;
};
