class MouseListener;

#pragma once
#include "Includes.h"
#include "MouseListener.h"

class MouseManager
{
    public:
        MouseManager();
        ~MouseManager();

        void MouseMoved(double aXPos, double aYPos);
        void MouseScrolled(double aYOffset);
        void AddListener(MouseListener* aListener);

        static MouseManager* GetMouseManager();

    private:
        std::vector<MouseListener*> oListeners;
        static MouseManager oMouseManager;

        double oPrevXPos = 0;
        double oPrevYPos = 0;
};
