#pragma once

enum LightType {
    DIRECTION_LIGHT,
    POINT_LIGHT,
    SPOT_LIGHT
};
