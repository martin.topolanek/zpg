#pragma once
#include "BaseObject.h"
#include "RawModel.h"
#include "AssimpModel.h"

class Object : public BaseObject
{
    public:
        Object();
        Object(std::string aModelID);

    protected:
        virtual void Draw() const;
};
