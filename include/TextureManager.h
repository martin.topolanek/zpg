#pragma once
#include "Includes.h"
#include "OpenGLIncludes.h"

class TextureManager
{
    public:
        TextureManager();

        static TextureManager* getTextureManager();

        void CreateTexture(std::string aPath);
        void CreateTexture(std::string aPath, std::string aTextureID);
        void CreateCubeTexture(std::vector<std::string> aFaces, std::string aTextureID = "CubeTexture");
        GLuint GetTexture(std::string aTextureID);

    private:
        static TextureManager oTextureManager;
        std::map<std::string, GLuint> oTextureIDs;

        GLuint SaveTexture(const char* aPath);
        GLuint SaveCubeTexture(std::vector<std::string> aFaces);
};
