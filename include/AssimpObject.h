#pragma once
#include "Object.h"
#include "Mesh.h"
#include "ModelManager.h"

class AssimpObject : public Object
{
    public:
        AssimpObject();
        AssimpObject(std::string aModelID);

    private:
        AssimpModel* oModel;
        std::vector<Mesh> oMeshes;

        virtual void Draw() const;
};

