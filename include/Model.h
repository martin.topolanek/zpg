#pragma once
#include "OpenGLIncludes.h"
#include "Includes.h"
#include "Types.h"
#include "TextureManager.h"



class Model
{
    public:
        Model();

    protected:
        GLuint oVAO, oVBO;
};
