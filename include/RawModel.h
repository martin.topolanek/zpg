#pragma once
#include "Model.h"
#include "Includes.h"
#include "TextureType.h"
#include "TextureManager.h"

class RawModel : public Model
{
    public:
        RawModel(std::vector<Vertex> aVertices);

        void AttachTexture(std::string aPath, TextureType aTextureType);
        GLuint GetTextureByType(TextureType aTextureType);
        GLuint GetVerticesAmount();
        GLuint GetVAO();

    private:
        std::vector<Vertex> oVertices;
        std::map<TextureType, GLuint> oTextures;
        GLuint oVerticesCount;

        void LoadModel(std::vector<Vertex> aVertices);
};
