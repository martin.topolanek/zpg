class KeyboardListener;

#pragma once
#include "KeyboardManager.h"

class KeyboardListener
{
    public:
        KeyboardListener();
        ~KeyboardListener();

        virtual void KeyPressed(int aKey);
        virtual void KeyReleased(int aKey);
};
