#ifndef SCENE_H
#define SCENE_H
#include <vector>
#include "Object.h"
#include "ModelManager.h"
#include "Shader.h"
#include "Camera.h"
#include "Skybox.h"

using namespace std;
class Scene
{
    public:
        Scene();
        static Scene* GetScene();

        void AddObject(BaseObject* aObject);
        void AddObject(BaseObject* aObject, std::string aShaderName);
        void AddObject(std::string aModelID);
        void AddLight(Light* aLight);
        void AddCamera(Camera* aCamera);
        void AddLightStorage(LightStorage* aLightStorage);
        void SetSkybox(Skybox* aSkybox);
        Skybox* GetSkybox();

        void CreateShader(const std::string aVertexShaderName, const std::string aFragmentShaderName, const std::string aProgramName);
        const Shader* GetShader(const std::string aShaderName);
        void Draw();

        void ClockTick();

        BaseObject* FindObject(BaseObject* aObject);

    protected:

    private:
        static Scene oScene;

        std::vector<Shader*> oShaders;
        std::vector<BaseObject*> oObjects;
        std::vector<Camera*> oCameras;
        LightStorage* oLightStorage;

        Camera* oActiveCamera;
        Skybox* oSkybox;

        BaseObject* FindObject(std::string aModelID);
};

#endif // SCENE_H
