class KeyboardManager;

#pragma once
#include "KeyboardListener.h"
#include "Includes.h"

class KeyboardManager
{
    public:
        KeyboardManager();
        ~KeyboardManager();

        void KeyPressed(int aKey);
        void KeyReleased(int aKey);
        void AddListener(KeyboardListener* aListener);
        bool IsPressed(int aKey);

        static KeyboardManager* GetKeyboardManager();

    protected:
        std::set<int> oPressedKeys;
        std::vector<KeyboardListener*> oListeners;
        static KeyboardManager oKeyboardManager;

    private:
};
