#pragma once

#ifndef TYPES
#define TYPES

#include "OpenGLIncludes.h"
#include "assimp/types.h"

typedef struct Position
{
    float X;
    float Y;
    float Z;
};

typedef struct Color
{
    float R;
    float G;
    float B;
    float A;

};

typedef struct VertexBase
{
    float Position[3];
    float Normal[3];
} VertexBase;

typedef struct Vertex
{
	float Position[3];
	float Normal[3];
	float TexCoords[2];
} Vertex;

typedef struct MeshVertex {
    glm::vec3 Position;
    glm::vec3 Normal;
    glm::vec2 TexCoords;
};

typedef struct Texture {
    GLuint Id;
    std::string Type;
    aiString Path;
};

#endif // TYPES
