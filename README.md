# About project
The project was created for the semester work in the subject Principles of computer graphic

# Getting started
In order to make this project work you will need:

 - Code::Blocks 16.01
 - Glew 2.1.0
 - GLFW 3.x
 - Assimp 3.3.1
 - SOIL